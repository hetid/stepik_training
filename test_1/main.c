#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define MAX_RANGE 9223372036854775807
#define MAX_LEN 32

int main(int argc, char const *argv[])
{
	char str[MAX_LEN];
	memset(str, 0, MAX_LEN);
	double triangular_num = 0;
	double result = 0;
	int res = 0;

	if (0 < scanf("%s", str)){
		for (int i = 0; i < MAX_LEN; ++i){
			if (isdigit(str[i]) || str[i] == '\0' || str[i] == '+') 
				continue; 
			else{ 
				printf("%d\n", res);
				return 0;
			}	
		}
	//	printf("строка %s\n", str);
		triangular_num = atof(str);
		if (fmod(triangular_num,1.0) == 0){
	//		printf("Введено : %lf\n", triangular_num);
			if ((triangular_num > 0) && (triangular_num <= MAX_RANGE)){
				result = (-1+powl((1+8*triangular_num), 1/2.0))/2;
				float rest = fmod(result, 1.0);
				if (rest != 0){
					result = 0;
				}
				res = result;
			}
		}
		
	}
	printf("%d\n", res);	
	return 0;
}